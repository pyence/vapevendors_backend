"""vapevendors db view functions"""
from rest_framework import viewsets, generics
from .serializers import VendorSerializer
from .models import Vendor


class VendorView(viewsets.ModelViewSet):
    """The view function for the Vendor model"""
    serializer_class = VendorSerializer
    queryset = Vendor.objects.all()
