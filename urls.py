""" Set up routing for this app's views 
    Include this in your Django project's main urls.py
"""

from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r"vendors", views.VendorView, "vapevendors")

urlpatterns = [
    path("api/", include(router.urls)),
]
