Vapevendors v0.1.0
==================

Early version of the Vapevendors backend (django app). Web interface and android app forthcoming

If you use this please send me a mail and let me know what you think, what you're missing, what you'd change, etc

requirements:
------------

Django  
For the API: Django Rest Framework

instructions:
------------

Set up your django project, clone this repo, and add it to your installed_apps

For the API: all of the above and: add `rest_framework` to your installed_apps and include `vapevendors_backend/urls.py` in your project's urls.py

If you need support, help setting this up, or just want to offer me a job (remote please!) don't hesitate to get in touch
