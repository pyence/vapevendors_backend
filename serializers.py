""" vapevendors/serializers.py will convert model instances to JSON"""
from rest_framework import serializers
from .models import Vendor


class VendorSerializer(serializers.ModelSerializer):
    """VendorSerializer converts Vendor data to JSON"""

    class Meta:
        model = Vendor
        fields = '__all__'
