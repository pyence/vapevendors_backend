"""models.py contains the datamodels used by the vapevendors Django project"""
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


class Vendor(models.Model):
    """ The Vendor model describes every vendor and its relevant information."""

    name = models.CharField("Vendor name", blank=False, max_length=50)
    url = models.URLField("Website", blank=False)
    purchaseMinimum = models.DecimalField(
        "Minimum purchase cost",
        blank=True,
        null=True,
        decimal_places=2,
        max_digits=5,
        help_text="Your purchase total must be at least this much to place an order",
    )
    deliveryTariff = models.DecimalField(
        "Delivery cost", blank=True, null=True, decimal_places=2, max_digits=5, help_text="In Euros"
    )
    deliveryTariff_Mid = models.DecimalField(
        "Medium tier delivery cost", blank=True, null=True, decimal_places=2, max_digits=5, help_text="In Euros"
    )
    deliveryTariff_High = models.DecimalField(
        "Maximum delivery cost", blank=True, null=True, decimal_places=2, max_digits=5, help_text="In Euros"
    )
    paypal = models.BooleanField("Accepts PayPal", default=True)
    freeDeliveryFrom = models.PositiveSmallIntegerField(
        "No-charge delivery minimum", blank=True, null=True, help_text="Minimum order total for free delivery"
    )
    nextday_OrderBefore = models.TimeField(
        "Next day delivery time", blank=True, null=True, help_text="Order before this time for next day delivery"
    )
    nextday_FridayOrderBefore = models.TimeField(
        "Next day delivery time (Friday)",
        blank=True,
        null=True,
        help_text="Order before this time on friday for delivery on saturday",
    )
    paypalcost_Fixed = models.DecimalField(
        "PayPal payment cost (fixed rate)",
        blank=True,
        null=True,
        decimal_places=2,
        max_digits=5,
        help_text="In Euros. This does not include the PayPal payment method markup percentage!",
    )
    paypalcost_Variable = models.DecimalField(
        "PayPal payment cost (percentage)",
        blank=True,
        null=True,
        decimal_places=2,
        max_digits=5,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text="In percent. This does not include the PayPal payment method markup fee!",
    )
    comments = models.TextField(blank=True)

    def __str__(self):
        return self.name
