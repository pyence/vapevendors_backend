"""vapevendors admin views"""
from django.contrib import admin
from .models import Vendor


def paypal_cost(Vendor):
    """paypal_cost returns True if the vendor charges extra for PayPal payment."""
    if Vendor.paypalcost_Variable and Vendor.paypalcost_Fixed:
        return f'{Vendor.paypalcost_Variable} % + € {Vendor.paypalcost_Fixed}'
    elif Vendor.paypalcost_Fixed:
        return f'€ {Vendor.paypalcost_Fixed}'
    elif Vendor.paypalcost_Variable:
        return f'{Vendor.paypalcost_Variable} %'


class VendorAdmin(admin.ModelAdmin):
    """Admin view of the Vendor db"""

    list_display = (
        "name",
        "paypal",
        paypal_cost,
        "deliveryTariff",
        "purchaseMinimum",
        "nextday_OrderBefore",
        "nextday_FridayOrderBefore",
        "url",
        "comments",
    )


admin.site.register(Vendor, VendorAdmin)
