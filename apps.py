from django.apps import AppConfig


class VapevendorsBackendConfig(AppConfig):
    name = 'vapevendors_backend'
